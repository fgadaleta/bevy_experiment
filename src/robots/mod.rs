use std::collections::HashSet;

use bevy::{
    // asset::{
        // AssetServerSettings,
        // LoadState
    // },
    // gltf::Gltf,
    // input::mouse::MouseMotion,
    // math::Vec3A,
    prelude::*,
    // render::mesh::{Indices, VertexAttributeValues},
    // render::{
        // camera::{
                // ActiveCamera,
                // Camera3d,
                // CameraProjection
            // },
        // primitives::{Aabb, Frustum, Sphere},
    // },
    // scene::InstanceId,
    math::{EulerRot, Quat, Vec3, Vec3Swizzles},
    // pbr::{DirectionalLight, DirectionalLightBundle, MaterialMeshBundle},
};
// use heron::RigidBody;
use heron::{
    *,
    rapier_plugin::{
        convert::IntoRapier, rapier3d::prelude::RigidBodySet, RigidBodyHandle
    }, Velocity,
    // CollisionLayers, CollisionShape, PhysicMaterial, PhysicsLayer, RigidBody,
};
// use bevy_asset_loader::{
    // AssetKeys,
    // AssetLoader,
    // AssetCollection,
    // DynamicAsset
// };
// use blender_bevy_toolkit::BlendLoadPlugin;
// use heron::{
    // rapier_plugin::{
        // nalgebra::Point3,
        // rapier3d::{math::Real, prelude::ColliderBuilder},
    // },
    // CollisionLayers,
    // CollisionShape,
    // CustomCollisionShape,
    // PhysicMaterial,
    // RigidBody,
    // PhysicsLayer
// };

// use crate::{
    // assets::{
        // custom_material::CustomMaterialFlags,
        // emissive_material::EmissiveMaterial,
        // light_shaft_material::{
            // update_light_shaft_material_time,
            // LightShaftMaterial,
            // LightShaftProperties,
        // },
        // ModelAssets,
    // },
    // assets::{
        // custom_material::{CustomMaterial, MaterialProperties, MaterialSetProp},
        // orb_material::update_orb_material_time,
        // GameState,
        // ImageAssets,
    // },
    // enemies::Waypoints,
    // ui::menu::GamePreferences,
    // Layer,
// };


// #[derive(Debug, Hash, PartialEq, Eq, Clone, SystemLabel)]
// struct CameraControllerCheckSystem;

// Component that will be used to tag entities in the scene
// #[derive(Component)]
// struct MeshedEntity;

#[derive(Component)]
pub struct Drone;


#[derive(Component)]
pub struct Obstacle;



pub struct FlyRobotsPlugin;

impl Plugin for FlyRobotsPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_system(fly_robots)
        ;
    }
}

fn fly_robots(
    time: Res<Time>,
    // mut timer: ResMut<EnemySpawnTimer>,
    mut commands: Commands,
    // settings: Res<MovementSettings>,
    mut rigid_bodies: ResMut<RigidBodySet>,
    // mut meshes: ResMut<Assets<Mesh>>,
    // mut query: ParamSet<(
    //     Query<(Entity, &Name, &mut Transform, &RigidBodyHandle), (Added<Name>, /*With<Drone>*/)>,
    // )>,
    // mut drones: Query<&mut Velocity, With<Drone>>,

    mut query: Query<
        (&Name, &mut Transform, &mut Velocity, &RigidBodyHandle), With<Drone>,
    >,
    // mut entities: Query<(Entity, &Name, &Transform, &RigidBodyHandle), (Added<Name>) >
    // waypoints: Res<Waypoints>,
    // enemies_state: Res<EnemiesState>,
    // enemies: Query<&Transform, (With<Drone>, Without<Player>, With<Alive>)>,
) {

    // for mut velocity in drones.iter_mut() {
    //     let speed = 10.;
    //     let target_velocity = Vec2::new(2., 2.).normalize_or_zero().extend(0.0) * speed;
    //     velocity.linear = target_velocity;
    // }


    for (name, mut transform, mut vel, rb) in query.iter_mut() {
            match name.as_str() {
             "uav3" => {
                    if let Some(body) = rigid_bodies.get_mut(rb.into_rapier()) {
                        let destination = transform.translation.xyz() + Vec3::new(0.,25.,0.);
                        let move_speed = 5.5; // enemy.move_speed;
                        // let dist = transform.translation.distance(destination);

                        let look_at = transform.looking_at(destination, transform.local_y()).forward();
                        // let tmp = Vec3::new(10.,10.,10.);
                        // dbg!(&tmp);
                        // let incremental_turn_weight = move_speed * time.delta_seconds();
                        // let old_rotation = transform.rotation;

                        // let rotation_delta = Quat::from_rotation_z(0.005 * move_speed * 1./60.);
                        // transform.rotation *= rotation_delta;

                        vel.linear = Vec3::Y; // * 300.0;
                        vel.angular = AxisAngle::new(Vec3::X, -100.);
                        dbg!(&name, &transform, &vel, &body.is_ccd_active());

                        // transform.rotation = old_rotation.lerp(look_at.rotation, incremental_turn_weight);

                        // let move_trans = look_at * (move_speed * (dist - 2.0)).min(move_speed).max(0.0);
                        // transform.translation += move_trans * time.delta_seconds();

                        // dbg!(&transform, &body.is_moving());
                        // transform.translation.z += 10.0 * time.delta_seconds();

                        /*
                        // let position = body.position();
                        dbg!(&position);
                        dbg!(&transform);
                            // waypoints.inside[enemy.current_destination] + enemy.current_random_offset;
                        // dbg!(&destination);
                        let dist = transform.translation.distance(destination);
                        dbg!(&dist);
                        let mut move_speed = 2.5; // enemy.move_speed;
                        if dist > 100.0 {
                            // enemies move faster if they have to go far.
                            move_speed *= 3.0;
                        }
                        let mut move_trans = transform.looking_at(destination, Vec3::Y).forward()
                            * (move_speed * (dist - 2.0)).min(move_speed).max(0.0);

                        move_trans =
                            Vec3::new(body.linvel().x, body.linvel().y, body.linvel().z).lerp(move_trans, 0.04);
                        // move_trans *= time.delta_seconds();
                        if !move_trans.is_finite() {
                            move_trans.x = 0.0;
                            move_trans.y = 0.0;
                            move_trans.z = 0.0;
                        }
                        body.set_linvel([move_trans.x, move_trans.y, move_trans.z].into(), false);
                        // dbg!(&body);

                        // body.set_rotation(transform.rotation, true);

                        // let target = transform.looking_at(destination, Vec3::Y);
                        // transform.rotation = transform.rotation.lerp(target.rotation, 0.14);


                        // if !enemy.within_range_of_player {
                        //     let target = enemy_transform.looking_at(destination, Vec3::Y);
                        //     enemy_transform.rotation = enemy_transform.rotation.lerp(target.rotation, 0.04);
                        // }
                    */
                    }
            }



            _ => {}
        }
    }









    return;
}


/*
fn enemies_move_to_destination(
    mut rigid_bodies: ResMut<RigidBodySet>,
    mut enemies: Query<
        (&mut Transform, &mut Enemy, &RigidBodyHandle),
        (Without<Player>, With<Alive>),
    >,
    waypoints: Res<Waypoints>,
) {
    for (mut enemy_transform, enemy, rb) in enemies.iter_mut() {
        if let Some(body) = rigid_bodies.get_mut(rb.into_rapier()) {
            let destination =
                waypoints.inside[enemy.current_destination] + enemy.current_random_offset;

            let dist = enemy_transform.translation.distance(destination);
            let mut move_speed = enemy.move_speed;
            if dist > 100.0 {
                // enemies move faster if they have to go far.
                move_speed *= 3.0;
            }
            let mut move_trans = enemy_transform.looking_at(destination, Vec3::Y).forward()
                * (move_speed * (dist - 2.0)).min(move_speed).max(0.0);

            move_trans =
                Vec3::new(body.linvel().x, body.linvel().y, body.linvel().z).lerp(move_trans, 0.04);
            if !move_trans.is_finite() {
                move_trans.x = 0.0;
                move_trans.y = 0.0;
                move_trans.z = 0.0;
            }
            body.set_linvel([move_trans.x, move_trans.y, move_trans.z].into(), false);

            if !enemy.within_range_of_player {
                let target = enemy_transform.looking_at(destination, Vec3::Y);
                enemy_transform.rotation = enemy_transform.rotation.lerp(target.rotation, 0.04);
            }
        }
    }
}
*/