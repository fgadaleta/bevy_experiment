use bevy::prelude::*;
use heron::{CollisionLayers, CollisionShape, PhysicMaterial, PhysicsLayer, RigidBody, RotationConstraints};

use crate::{
    assets::{
        // orb_material::{OrbMaterial, OrbProperties},
        // AudioAssets,
        // ModelAssets,
    },
    player::Player,
    world::LevelAsset,
    Layer,
};

use super::{
    bullet::BulletBundle,
    Alive,
    EnemiesState,
    Enemy,
    // EnemyBehaviour,
    EnemyLastFired
};

#[derive(Component)]
pub struct Drone {
    pub health: f32,
    pub max_health: f32,
}

impl Default for Drone {
    fn default() -> Self {
        Drone {
            health: 1000.0,
            max_health: 1000.0,
        }
    }
}

// use bevy_kira_audio::Audio;
#[derive(Bundle)]
struct DroneBundle {
    player: Drone,
    transform: Transform,
    global_tranform: GlobalTransform,
    rigid_body: RigidBody,
    collision_layers: CollisionLayers,
    collision_shape: CollisionShape,
    rotation_constraints: RotationConstraints,
    physic_material: PhysicMaterial,
}

impl Default for DroneBundle {
    fn default() -> Self {
        DroneBundle {
            player: Drone::default(),
            transform: Transform::from_xyz(0.0, 1.0, 60.0),
            global_tranform: GlobalTransform::default(),
            rigid_body: RigidBody::Dynamic,
            collision_layers: CollisionLayers::from_bits(
                Layer::Drone.to_bits(),
                Layer::all_bits(),
            )
            .with_group(Layer::Player)
            .with_masks([Layer::Bullet, Layer::Enemy, Layer::World]),
            collision_shape: CollisionShape::Capsule {
                half_segment: 1.0,
                radius: 0.5,
            },
            rotation_constraints: RotationConstraints::lock(),
            physic_material: PhysicMaterial {
                restitution: 0.1,
                density: 10.0,
                friction: 0.5,
            },
        }
    }
}


// #[derive(Component, Default)]
// pub struct DroneEnemy;

// impl EnemyBehaviour for DroneEnemy {
//     fn spawn(commands: &mut Commands, transform: Transform, model_assets: &ModelAssets) -> Entity {
//         commands
//             .spawn_bundle((transform, GlobalTransform::default()))
//             .insert(RigidBody::Dynamic)
//             .insert(CollisionShape::Sphere { radius: 5.7 })
//             .insert(CollisionLayers::from_bits(
//                 Layer::Enemy.to_bits(),
//                 Layer::all_bits(),
//             ))
//             .insert(PhysicMaterial {
//                 density: 30.0, // Value must be greater than 0.0
//                 ..Default::default()
//             })
//             .insert(EnemyLastFired(Timer::from_seconds(0.9, true)))
//             .insert(Enemy {
//                 health: 1000,
//                 range: 100.0,
//                 update_destination_timer: Timer::from_seconds(2.0, true),
//                 move_speed: 30.0,
//                 weapon_damage: 40.0,
//                 weapon_splash_radius: 8.0,
//                 rotate_lerp: 0.9,
//                 ..Default::default()
//             })
//             .insert(DroneEnemy)
//             .insert(Alive)
//             .with_children(|parent| {
//                 parent.spawn_scene(model_assets.drone.clone());
//             })
//             .id()
//     }
// }

/*
pub fn drone_enemies_fire_at_player(
    mut commands: Commands,
    time: Res<Time>,
    mut enemies: Query<
        (&Transform, &mut EnemyLastFired, &mut Enemy),
        (With<Alive>, With<DroneEnemy>),
    >,
    mut orb_materials: ResMut<Assets<OrbMaterial>>,
    enemies_state: Res<EnemiesState>,
    mut meshes: ResMut<Assets<Mesh>>,
    audio: Res<Audio>,
    // audio_assets: Res<AudioAssets>,
    player: Query<&Player>,
) {
    if let Some(player) = player.iter().next() {
        if player.health <= 0.0 {
            return;
        }
    }
    for (transform, mut enemy_last_fired, enemy) in enemies.iter_mut() {
        enemy_last_fired.0.tick(time.delta());
        if enemy_last_fired.0.just_finished() && enemy.within_range_of_player {
            // Shoot at player
            commands
                .spawn_bundle(BulletBundle::shoot(
                    transform.translation,
                    transform.forward(),
                    enemy.weapon_damage as f32 * enemies_state.get_level_params().damage_multiplier,
                    enemy.weapon_splash_radius,
                ))
                .with_children(|parent| {
                    // // Debug hit box
                    let orb_material_props = OrbProperties {
                        color_tint: Vec3::new(0.5, 0.5, 1.0),
                        radius: 0.0,
                        inner_radius: 0.28,
                        alpha: 1.0,
                        ..Default::default()
                    };
                    let orb_material = orb_materials.add(OrbMaterial {
                        material_properties: orb_material_props,
                        noise_texture: None,
                    });
                    let mesh = meshes.add(Mesh::from(shape::Icosphere {
                        radius: 2.0,
                        subdivisions: 1,
                    })); //TODO use billboard
                    parent
                        .spawn()
                        .insert_bundle(MaterialMeshBundle {
                            mesh,
                            transform: Transform::from_xyz(0.0, 0.0, 0.0),
                            material: orb_material.clone(),
                            ..Default::default()
                        })
                        .insert(LevelAsset::OrbMaterial {
                            properties: orb_material_props,
                            handle: orb_material,
                        });
                });
            // TODO use event
            // audio.play(audio_assets.get_unit2_fire().clone());
        }
    }
}
*/