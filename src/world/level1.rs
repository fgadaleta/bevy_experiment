use bevy::{
    asset::{
        // AssetServerSettings,
        LoadState
    },
    gltf::{
        Gltf,
        GltfMesh,
        GltfNode
    },
    // input::mouse::MouseMotion,
    // math::Vec3A,
    prelude::*,
    // render::mesh::{
        // Indices,
        // VertexAttributeValues
    // },
    // render::{
        // camera::{
                // ActiveCamera,
                // Camera3d,
                // CameraProjection
            // },
        // primitives::{Aabb, Frustum, Sphere},
    // },

    scene::InstanceId,
    math::{
        // EulerRot,
        // Quat,
        Vec3
    },
    pbr::{
        DirectionalLight,
        // DirectionalLightBundle,
        // MaterialMeshBundle
    },
};
use bevy_animation::AnimationClip;
use std::collections::HashSet;
// use bevy_asset_loader::{
    // AssetKeys,
    // AssetLoader,
    // AssetCollection,
    // DynamicAsset
// };
// use blender_bevy_toolkit::BlendLoadPlugin;
use heron::{
    *,
    // rapier_plugin::{
        // nalgebra::Point3,
        // rapier3d::{
                // math::Real,
                // prelude::
                                    // {
                                        // ColliderBuilder,
                                        // RigidBodySet
                                    // }
                // },
    // },
    CollisionLayers,
    CollisionShape,
    // CustomCollisionShape,
    PhysicMaterial,
    RigidBody,
    // PhysicsLayer
};

use heron::{
    rapier_plugin::{
        convert::IntoRapier, rapier3d::prelude::RigidBodySet, RigidBodyHandle
    },
    Velocity,
    // CollisionLayers, CollisionShape, PhysicMaterial, PhysicsLayer, RigidBody,
};


// use std::f32::consts::PI;
use heron::CollisionEvent;

use crate::{
    robots::{Drone, Obstacle},
    assets::{
        custom_material::CustomMaterialFlags,
        // emissive_material::EmissiveMaterial,
        // light_shaft_material::{
            // update_light_shaft_material_time,
            // LightShaftMaterial,
            // LightShaftProperties,
        // },
        // ModelAssets,
    },
    assets::{
        // custom_material::{CustomMaterial, MaterialProperties, MaterialSetProp},
        // orb_material::update_orb_material_time,
        GameState,
        // ImageAssets,
    },
    // enemies::Waypoints,
    ui::menu::GamePreferences,
    Layer,
};

// use super::LevelAsset;

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemLabel)]
struct CameraControllerCheckSystem;

// Component that will be used to tag entities in the scene
#[derive(Component)]
struct MeshedEntity;


pub struct LevelOnePlugin;

impl Plugin for LevelOnePlugin {
    fn build(&self, app: &mut App) {
        app
        .add_startup_system(setup)
        .add_system_set(
            SystemSet::on_enter(GameState::Playing)
                .with_system(setup_level_one)
                // .with_system(spawn_demo_cubes)
                // .with_system(spawn_cool_drone)
                // .with_system(spawn_waypoints),
        )
        .add_system_to_stage(CoreStage::PreUpdate, scene_load_check)
        // .add_system(assign_colliders_to_meshed_entities)
        // .add_system_to_stage(CoreStage::PreUpdate, camera_spawn_check)
        // .add_system(camera_controller)
        // .add_system(update_light_shaft_material_time)
        // .add_system(update_orb_material_time)
        // .add_system(gltf_manual_entity)
        .add_system(assign_components)
        .add_system(enable_ccd)
        // .add_system(log_collisions)
        ;
    }
}

#[allow(dead_code)]
fn spawn_demo_cubes(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    preferences: Res<GamePreferences>,
) {
    if !preferences.potato {
        let mesh = meshes.add(Mesh::from(shape::Cube { size: 3.0 }));
        let material = materials.add(StandardMaterial {
            base_color: Color::DARK_GRAY,
            ..Default::default()
        });

        for i in 0..3 {
            commands
                .spawn_bundle(PbrBundle {
                    mesh: mesh.clone(),
                    material: material.clone(),
                    transform: Transform::from_xyz(0.0, i as f32 * 1. + 5.0, 30.0),
                    ..Default::default()
                })
                .insert(RigidBody::Dynamic)
                .insert(CollisionShape::Cuboid {
                    half_extends: Vec3::new(1.5, 1.5, 1.5),
                    border_radius: None,
                })
                .insert(PhysicMaterial {
                    restitution: 0.7,
                    ..Default::default()
                })
                .insert(
                    CollisionLayers::none()
                        .with_group(Layer::World)
                        .with_masks(Layer::all()),
                );
        }
    }
}


// #[allow(dead_code)]
// fn spawn_cool_drone(
//     mut commands: Commands,
//     // mut meshes: ResMut<Assets<Mesh>>,
//     mut materials: ResMut<Assets<StandardMaterial>>,
//     // model_assets: Res<ModelAssets>,
//     preferences: Res<GamePreferences>,
// ) {
//     if !preferences.potato {
//         // let mesh = meshes.add(Mesh::from(shape::Cube { size: 3.0 }));
//         let material = materials.add(StandardMaterial {
//             base_color: Color::DARK_GRAY,
//             ..Default::default()
//         });
//
//         commands
//             .spawn_bundle(PbrBundle {
//                 // mesh: mesh.clone(),
//                 material: material.clone(),
//                 transform: Transform::from_xyz(0.0, 0., -5.0),
//                 ..Default::default()
//             })
//             .insert(RigidBody::Dynamic)
//
//             .insert(CollisionShape::Sphere { radius: 2.7 * 3.0 })
//
//             .insert(PhysicMaterial {
//                 density: 10.,
//                 restitution: 0.5,
//                 ..Default::default()
//             })
//
//             .insert(CollisionLayers::from_bits(
//                 Layer::Enemy.to_bits(),
//                 Layer::all_bits(),
//             ))
//
//             .with_children(|parent| {
//                 parent.spawn_scene(model_assets.drone.clone());
//             });
//     }
// }


// pub fn set_textures_res(mut asset_keys: ResMut<AssetKeys>, high_res: bool) {
//     let sm = if high_res { "" } else { "sm/" };
//     for s in [
//         "large_ceiling_supports",
//         "pillars",
//         "sky_box",
//         "spheres_base",
//         "spheres",
//         "walls",
//         // "beaver_county_airport"
//     ] {
//         asset_keys.register_asset(
//             &format!("level1_{}", s),
//             DynamicAsset::File {
//                 path: format!("textures/level1/bake/{}{}.jpg", sm, s),
//             },
//         );
//     }
//     for s in ["concrete", "concrete3", "detail", "reflection"] {
//         asset_keys.register_asset(
//             s,
//             DynamicAsset::File {
//                 path: format!("textures/{}{}.jpg", sm, s),
//             },
//         );
//     }
// }


struct SceneHandle {
    handle: Handle<Scene>,
    animations: Vec<Handle<AnimationClip>>,
    instance_id: Option<InstanceId>,
    is_loaded: bool,
    has_camera: bool,
    has_light: bool,
}


fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    // Load scene
    let scene_path = "../assets/simple_hangar.gltf".to_string();
    info!("Loading scene {}", scene_path);
    let gltf_scene = asset_server.load(&scene_path);

    commands
    .insert_resource(SceneHandle {
        handle: gltf_scene,
        animations: Vec::new(),
        instance_id: None,
        is_loaded: false,
        has_camera: false,
        has_light: false,
    });
}



fn setup_level_one(
    mut commands: Commands,
    // mesh_assets: Res<Assets<Mesh>>,
    // image_assets: Res<ImageAssets>,
    // model_assets: Res<ModelAssets>,
    // asset_server: Res<AssetServer>,
    // mut custom_materials: ResMut<Assets<CustomMaterial>>,
    // mut emissive_materials: ResMut<Assets<EmissiveMaterial>>,
    // mut light_shaft_materials: ResMut<Assets<LightShaftMaterial>>,
    preferences: Res<GamePreferences>,
) {

    // let variation_texture = image_assets.detail.clone();
    // let base_texture = image_assets.concrete.clone();
    // let walls_texture = image_assets.concrete3.clone();
    // let reflection_texture = image_assets.reflection.clone();

    let mut flags = CustomMaterialFlags::NONE;
    if preferences.dynamic_shadows {
        flags |= CustomMaterialFlags::SHADOWS;
    }
    if preferences.potato {
        flags |= CustomMaterialFlags::POTATO;
    }
    // let material_properties = MaterialProperties {
    //     lightmap: MaterialSetProp {
    //         scale: 1.0,
    //         contrast: 2.3,
    //         brightness: 2.5,
    //         blend: 1.0,
    //     },
    //     base_a: MaterialSetProp {
    //         scale: 8.5,
    //         contrast: 0.33,
    //         brightness: 2.5,
    //         blend: 1.0,
    //     },
    //     base_b: MaterialSetProp {
    //         scale: 33.0,
    //         contrast: 0.3,
    //         brightness: 2.2,
    //         blend: 1.0,
    //     },
    //     vary_a: MaterialSetProp {
    //         scale: 0.14,
    //         contrast: 0.77,
    //         brightness: 4.2,
    //         blend: 0.04,
    //     },
    //     vary_b: MaterialSetProp {
    //         scale: 70.0,
    //         contrast: 0.105,
    //         brightness: 1.35,
    //         blend: 1.0,
    //     },
    //     reflection: MaterialSetProp {
    //         scale: 1.0,
    //         contrast: 1.2,
    //         brightness: 0.023,
    //         blend: 1.0,
    //     },
    //     walls: MaterialSetProp {
    //         scale: 20.0,
    //         contrast: 0.53,
    //         brightness: 1.6,
    //         blend: 1.0,
    //     },
    //     reflection_mask: MaterialSetProp {
    //         scale: 0.047,
    //         contrast: 2.5,
    //         brightness: 40.0,
    //         blend: 1.0,
    //     },
    //     mist: MaterialSetProp {
    //         scale: 0.03,
    //         contrast: 1.0,
    //         brightness: 0.5,
    //         blend: 1.0,
    //     },
    //     directional_light_blend: 0.6,
    //     flags: flags.bits(),
    // };

    // Skybox
    // let skybox_model = model_assets.level1_sky_box.clone();
    // let skybox_texture = image_assets.level1_sky_box.clone();
    // commands.spawn().insert_bundle(MaterialMeshBundle {
    //     mesh: skybox_model,
    //     transform: Transform::from_xyz(0.0, 0.0, 0.0).with_scale(Vec3::new(10.0, 10.0, 10.0)),
    //     material: emissive_materials.add(EmissiveMaterial {
    //         emissive: Color::WHITE,
    //         emissive_texture: Some(skybox_texture),
    //     }),
    //     ..Default::default()
    // });

    if preferences.light_shafts {
        // let light_shaft_material_props = LightShaftProperties {
        //     shaft: MaterialSetProp {
        //         scale: 1.0,
        //         contrast: 1.9,
        //         brightness: 8.0,
        //         blend: 1.0,
        //     },
        //     noise_a: MaterialSetProp {
        //         scale: 1.0,
        //         contrast: 5.8,
        //         brightness: 40.0,
        //         blend: 0.25,
        //     },
        //     noise_b: MaterialSetProp {
        //         scale: 0.00048,
        //         contrast: 1.3,
        //         brightness: 3.6,
        //         blend: 1.0,
        //     },
        //     speed: Vec3::new(-0.004, -0.01, 0.0),
        //     color_tint: Vec3::new(1.0, 0.783, 0.57),
        //     time: 0.0,
        // };
        // let light_shaft_material = light_shaft_materials.add(LightShaftMaterial {
        //     noise_texture: Some(variation_texture.clone()),
        //     material_properties: light_shaft_material_props,
        // });
        // let light_shaft_model = &model_assets.level1_light_shafts;

        // commands
        //     .spawn()
        //     .insert_bundle(MaterialMeshBundle {
        //         mesh: light_shaft_model.clone(),
        //         transform: Transform::from_xyz(0.0, 0.0, 0.0),
        //         material: light_shaft_material.clone(),
        //         ..Default::default()
        //     })
        //     .insert(LevelAsset::LightShaftMaterial {
        //         properties: light_shaft_material_props,
        //         handle: light_shaft_material,
        //     });
    }

/*
    for (model, lightbake) in [
        (model_assets.level1_walls.clone(), image_assets.level1_walls.clone(),),
        (model_assets.floor.clone(), image_assets.concrete.clone()),
        // (model_assets.level1_pillars.clone(), image_assets.level1_pillars.clone(),),
        // (
        //     model_assets.level1_spheres.clone(),
        //     image_assets.level1_spheres.clone(),
        // ),
        // (
        //     model_assets.level1_large_ceiling_supports.clone(),
        //     image_assets.level1_large_ceiling_supports.clone(),
        // ),
        // (
        //     model_assets.level1_spheres_base.clone(),
        //     image_assets.level1_spheres_base.clone(),
        // ),
    ] {
        let material = custom_materials.add(CustomMaterial {
            material_properties,
            textures: [
                lightbake,
                base_texture.clone(),
                variation_texture.clone(),
                reflection_texture.clone(),
                walls_texture.clone(),
            ],
        });

        let mesh = mesh_assets.get(model.clone()).unwrap();

        let vertices: Vec<Point3<Real>> = match mesh.attribute(Mesh::ATTRIBUTE_POSITION) {
            None => panic!("Mesh does not contain vertex positions"),
            Some(vertex_values) => match &vertex_values {
                VertexAttributeValues::Float32x3(positions) => positions
                    .iter()
                    .map(|[x, y, z]| Point3::new(*x, *y, *z))
                    .collect(),
                _ => panic!("Unexpected types in {:?}", Mesh::ATTRIBUTE_POSITION),
            },
        };

        let indices: Vec<_> = match mesh.indices().unwrap() {
            Indices::U16(_) => {
                panic!("expected u32 indices");
            }
            Indices::U32(indices) => indices
                .chunks(3)
                .map(|chunk| [chunk[0], chunk[1], chunk[2]])
                .collect(),
        };

        commands
            .spawn()
            .insert_bundle(MaterialMeshBundle {
                mesh: model,
                transform: Transform::from_xyz(0.0, 0.0, 0.0),
                material: material.clone(),
                ..Default::default()
            })
            .insert(RigidBody::Static)
            .insert(CollisionShape::Custom {
                shape: CustomCollisionShape::new(ColliderBuilder::trimesh(vertices, indices)),
            })
            .insert(LevelAsset::CustomMaterial {
                properties: material_properties,
                handle: material,
            })
            .insert(
                CollisionLayers::none()
                    .with_group(Layer::World)
                    .with_masks(Layer::all()),
            );
    }



    if preferences.dynamic_shadows {
        //Bevy Sun
        let size: f32 = 120.0;
        let sun_rot_x = -67.0f32;
        let sun_rot_y = 22.0f32;
        //8.0f32;
        commands.spawn_bundle(DirectionalLightBundle {
            directional_light: DirectionalLight {
                // Configure the projection to better fit the scene
                shadow_projection: OrthographicProjection {
                    left: -size * 4.0,
                    right: size * 2.0,
                    bottom: -size * 2.0,
                    top: size * 2.0,
                    near: -size * 2.0,
                    far: size * 1.0,
                    ..Default::default()
                },
                illuminance: 100000.0,
                shadows_enabled: true,
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, 0.0, 0.0),
                rotation: Quat::from_euler(
                    EulerRot::XYZ,
                    (sun_rot_y).to_radians(),
                    -(sun_rot_x - 180.0f32).to_radians(),
                    0.0,
                ),
                ..Default::default()
            },
            ..Default::default()
        });
    }
    */

    // Sky Light for Bevy PBR
    commands.spawn_bundle(PointLightBundle {
        transform: Transform::from_xyz(0.0, 15.0, -200.0),
        point_light: PointLight {
            intensity: 1000000.0,
            range: 1000.0,
            radius: 100.0,
            color: Color::rgb(0.95, 0.95, 0.45),
            shadows_enabled: false,
            ..Default::default()
        },
        ..Default::default()
    });

    commands.spawn_bundle(PointLightBundle {
        transform: Transform::from_xyz(0.0, 15.0, 200.0),
        point_light: PointLight {
            intensity: 1000000.0,
            range: 1000.0,
            radius: 100.0,
            color: Color::rgb(0.95, 0.95, 0.45),
            shadows_enabled: false,
            ..Default::default()
        },
        ..Default::default()
    });
}



/*
fn gltf_manual_entity(
    mut commands: Commands,
    scene: Res<SceneHandle>,
    assets_gltf: Res<Assets<Gltf>>,
    assets_gltfmesh: Res<Assets<GltfMesh>>,
) {
    if let Some(gltf) = assets_gltf.get(&scene.handle) {
        // Get the GLTF Mesh named "CarWheel"
        // (unwrap safety: we know the GLTF has loaded already)

        // for (name, _) in &gltf.named_nodes {
        //     if name.eq(&"uav1".to_string()) {
                // dbg!(&name);
                // let uav1 = assets_gltfmesh.get(&gltf.named_nodes["uav1"]); //.unwrap();
                // dbg!(&uav1);
        //     }
        // }


        // let uav1 = assets_gltfmesh.get(&gltf.named_nodes["uav1"]).unwrap();
        // // // Spawn a PBR entity with the mesh and material of the first GLTF Primitive
        // commands.spawn_bundle(PbrBundle {
        //     mesh: uav1.primitives[0].mesh.clone(),
        //     // (unwrap: material is optional, we assume this primitive has one)
        //     material: uav1.primitives[0].material.clone().unwrap(),
        //     ..Default::default()
        // });
    }
}
*/


// fn spawn_waypoints(
//     //mut commands: Commands,
//     //mut meshes: ResMut<Assets<Mesh>>,
//     //mut materials: ResMut<Assets<StandardMaterial>>,
//     mut waypoints: ResMut<Waypoints>,
// ) {
//     //--- WAYPOINTS ---
//     //--- inside ---
//     waypoints.inside = vec![
//         Vec3::new(-10.0, 36.4119, -210.641),
//         Vec3::new(10.0, 36.4119, -210.641),
//         Vec3::new(32.0, 36.4119, -211.335),
//         Vec3::new(-32.0, 36.4119, -210.988),
//         Vec3::new(-17.0, 36.4119, -170.228),
//         Vec3::new(17.0, 36.4119, -169.361),
//         Vec3::new(-17.0, 36.4119, -129.468),
//         Vec3::new(17.0, 36.4119, -129.468),
//         Vec3::new(-17.0, 36.4119, -89.2278),
//         Vec3::new(17.0, 36.4119, -89.2278),
//         Vec3::new(-17.0, 36.4119, -50.0285),
//         Vec3::new(17.0, 36.4119, -50.0285),
//         Vec3::new(-17.0, 36.4119, -9.44168),
//         Vec3::new(17.0, 36.4119, -9.44168),
//         Vec3::new(-17.0, 36.4119, 29.0638),
//         Vec3::new(17.0, 36.4119, 29.0638),
//         Vec3::new(-17.0, 36.4119, 70.3444),
//         Vec3::new(17.0, 36.4119, 70.3444),
//         Vec3::new(-10.0, 36.4119, 109.891),
//         Vec3::new(10.0, 36.4119, 109.891),
//         Vec3::new(10.0, 36.4119, -190.175),
//         Vec3::new(-10.0, 36.4119, -190.521),
//         Vec3::new(10.0, 36.4119, -150.281),
//         Vec3::new(-10.0, 36.4119, -150.628),
//         Vec3::new(10.0, 36.4119, -110.042),
//         Vec3::new(-10.0, 36.4119, -110.388),
//         Vec3::new(10.0, 36.4119, -69.8016),
//         Vec3::new(-10.0, 36.4119, -70.1485),
//         Vec3::new(10.0, 36.4119, -29.5616),
//         Vec3::new(-10.0, 36.4119, -29.9085),
//         Vec3::new(10.0, 36.4119, 9.63762),
//         Vec3::new(-10.0, 36.4119, 9.29073),
//         Vec3::new(10.0, 36.4119, 50.2245),
//         Vec3::new(-10.0, 36.4119, 49.8776),
//         Vec3::new(10.0, 36.4119, 90.1175),
//         Vec3::new(-10.0, 36.4119, 89.7706),
//         Vec3::new(-10.0, 36.4119, -230.636),
//         Vec3::new(10.0, 36.4119, -230.636),
//         Vec3::new(32.0, 36.4119, -231.33),
//         Vec3::new(-32.0, 36.4119, -230.983),
//         Vec3::new(-10.0, 36.4119, -250.39),
//         Vec3::new(10.0, 36.4119, -250.39),
//         Vec3::new(32.0, 36.4119, -251.084),
//         Vec3::new(-32.0, 36.4119, -250.737),
//     ];
//     //--- outside ---
//     waypoints.outside = vec![
//         Vec3::new(-80.0, 36.4119, -130.231),
//         Vec3::new(-80.0, 36.4119, -169.869),
//         Vec3::new(-80.0, 36.4119, -90.3362),
//         Vec3::new(-80.0, 36.4119, -50.1839),
//         Vec3::new(-80.0, 36.4119, 30.6253),
//         Vec3::new(-80.0, 36.4119, -9.01227),
//         Vec3::new(80.0, 36.4119, -169.869),
//         Vec3::new(80.0, 36.4119, -90.3362),
//         Vec3::new(80.0, 36.4119, -50.1839),
//         Vec3::new(80.0, 36.4119, 30.6253),
//         Vec3::new(80.0, 36.4119, -9.01227),
//         Vec3::new(80.0, 36.4119, 70.5203),
//         Vec3::new(-80.0, 36.4119, 70.5203),
//         Vec3::new(80.0, 36.4119, -130.231),
//     ];
//     //--- window ---
//     waypoints.window = vec![
//         Vec3::new(49.4907, 36.4119, -129.961),
//         Vec3::new(49.4907, 36.4119, -170.024),
//         Vec3::new(49.4907, 36.4119, -90.012),
//         Vec3::new(49.7481, 36.4119, -49.9812),
//         Vec3::new(49.4907, 36.4119, 30.0028),
//         Vec3::new(49.4907, 36.4119, -10.0627),
//         Vec3::new(49.4907, 36.4119, 69.9123),
//         Vec3::new(-49.4163, 36.4119, -129.983),
//         Vec3::new(-49.4163, 36.4119, -170.102),
//         Vec3::new(-49.4163, 36.4119, -89.9796),
//         Vec3::new(-49.4291, 36.4119, -49.9677),
//         Vec3::new(-49.4163, 36.4119, 30.0093),
//         Vec3::new(-49.4163, 36.4119, -10.0394),
//         Vec3::new(-49.4163, 36.4119, 70.0339),
//     ];
//     //--- outfront ---
//     waypoints.outfront = vec![
//         Vec3::new(-42.1961, 36.4119, -541.816),
//         Vec3::new(44.0373, 36.4119, -569.029),
//         Vec3::new(-10.0, 36.4119, -272.966),
//         Vec3::new(10.0, 36.4119, -272.966),
//         Vec3::new(-42.6957, 36.4119, -568.458),
//         Vec3::new(44.5535, 36.4119, -541.129),
//         Vec3::new(43.9461, 36.4119, -599.505),
//         Vec3::new(-42.9969, 36.4119, -599.29),
//     ];
//     /*
//     let mesh = meshes.add(Mesh::from(shape::UVSphere {
//         radius: 0.5,
//         sectors: 8,
//         stacks: 8,
//     }));
//     let material = materials.add(StandardMaterial {
//         base_color: Color::BLUE,
//         ..Default::default()
//     });
//
//     for pos in waypoints.inside.iter() {
//         commands
//             .spawn_bundle(PbrBundle {
//                 mesh: mesh.clone(),
//                 material: material.clone(),
//                 transform: Transform::from_xyz(pos.x, pos.y, pos.z),
//                 ..Default::default()
//             })
//             .insert(Waypoint);
//     }
//     for pos in waypoints.outside.iter() {
//         commands
//             .spawn_bundle(PbrBundle {
//                 mesh: mesh.clone(),
//                 material: material.clone(),
//                 transform: Transform::from_xyz(pos.x, pos.y, pos.z),
//                 ..Default::default()
//             })
//             .insert(Waypoint);
//     }
//     for pos in waypoints.window.iter() {
//         commands
//             .spawn_bundle(PbrBundle {
//                 mesh: mesh.clone(),
//                 material: material.clone(),
//                 transform: Transform::from_xyz(pos.x, pos.y, pos.z),
//                 ..Default::default()
//             })
//             .insert(Waypoint);
//     }
//     for pos in waypoints.outfront.iter() {
//         commands
//             .spawn_bundle(PbrBundle {
//                 mesh: mesh.clone(),
//                 material: material.clone(),
//                 transform: Transform::from_xyz(pos.x, pos.y, pos.z),
//                 ..Default::default()
//             })
//             .insert(Waypoint);
//     }*/
// }


// #[derive(Component)]
// struct CameraController {
//     pub enabled: bool,
//     pub initialized: bool,
//     pub sensitivity: f32,
//     pub key_forward: KeyCode,
//     pub key_back: KeyCode,
//     pub key_left: KeyCode,
//     pub key_right: KeyCode,
//     pub key_up: KeyCode,
//     pub key_down: KeyCode,
//     pub key_run: KeyCode,
//     pub key_enable_mouse: MouseButton,
//     pub walk_speed: f32,
//     pub run_speed: f32,
//     pub friction: f32,
//     pub pitch: f32,
//     pub yaw: f32,
//     pub velocity: Vec3,
// }

// impl Default for CameraController {
//     fn default() -> Self {
//         Self {
//             enabled: true,
//             initialized: false,
//             sensitivity: 0.5,
//             key_forward: KeyCode::W,
//             key_back: KeyCode::S,
//             key_left: KeyCode::A,
//             key_right: KeyCode::D,
//             key_up: KeyCode::E,
//             key_down: KeyCode::Q,
//             key_run: KeyCode::LShift,
//             key_enable_mouse: MouseButton::Left,
//             walk_speed: 5.0,
//             run_speed: 15.0,
//             friction: 0.5,
//             pitch: 0.0,
//             yaw: 0.0,
//             velocity: Vec3::ZERO,
//         }
//     }
// }

/*
fn camera_controller(
    time: Res<Time>,
    mut mouse_events: EventReader<MouseMotion>,
    mouse_button_input: Res<Input<MouseButton>>,
    key_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut Transform, &mut CameraController), With<Camera>>,
) {

    let dt = time.delta_seconds();

    if let Ok((mut transform, mut options)) = query.get_single_mut() {
        if !options.initialized {
            let (yaw, pitch, _roll) = transform.rotation.to_euler(EulerRot::YXZ);
            options.yaw = yaw;
            options.pitch = pitch;
            options.initialized = true;
        }
        if !options.enabled {
            return;
        }

        // Handle key input
        let mut axis_input = Vec3::ZERO;
        if key_input.pressed(options.key_forward) {
            axis_input.z += 1.0;
        }
        if key_input.pressed(options.key_back) {
            axis_input.z -= 1.0;
        }
        if key_input.pressed(options.key_right) {
            axis_input.x += 1.0;
        }
        if key_input.pressed(options.key_left) {
            axis_input.x -= 1.0;
        }
        if key_input.pressed(options.key_up) {
            axis_input.y += 1.0;
        }
        if key_input.pressed(options.key_down) {
            axis_input.y -= 1.0;
        }

        // Apply movement update
        if axis_input != Vec3::ZERO {
            let max_speed = if key_input.pressed(options.key_run) {
                options.run_speed
            } else {
                options.walk_speed
            };
            options.velocity = axis_input.normalize() * max_speed;
        } else {
            let friction = options.friction.clamp(0.0, 1.0);
            options.velocity *= 1.0 - friction;
            if options.velocity.length_squared() < 1e-6 {
                options.velocity = Vec3::ZERO;
            }
        }
        let forward = transform.forward();
        let right = transform.right();
        transform.translation += options.velocity.x * dt * right
            + options.velocity.y * dt * Vec3::Y
            + options.velocity.z * dt * forward;

        // Handle mouse input
        let mut mouse_delta = Vec2::ZERO;
        if mouse_button_input.pressed(options.key_enable_mouse) {
            for mouse_event in mouse_events.iter() {
                mouse_delta += mouse_event.delta;
            }
        }

        if mouse_delta != Vec2::ZERO {
            // Apply look update
            let (pitch, yaw) = (
                (options.pitch - mouse_delta.y * 0.5 * options.sensitivity * dt).clamp(
                    -0.99 * std::f32::consts::FRAC_PI_2,
                    0.99 * std::f32::consts::FRAC_PI_2,
                ),
                options.yaw - mouse_delta.x * options.sensitivity * dt,
            );
            transform.rotation = Quat::from_euler(EulerRot::ZYX, 0.0, yaw, pitch);
            options.pitch = pitch;
            options.yaw = yaw;
        }
    }
}
*/

/*
fn camera_spawn_check(
    mut commands: Commands,
    mut scene_handle: ResMut<SceneHandle>,
    meshes: Query<(&GlobalTransform, Option<&Aabb>), With<Handle<Mesh>>>,
    cameras_3d: Query<(&GlobalTransform, &Camera), With<Camera3d>>,
    mut active_camera_3d: ResMut<ActiveCamera<Camera3d>>,
) {
    // If the scene did not contain a camera, find an approximate bounding box of the scene from
    // its meshes and spawn a camera that fits it in view
    if scene_handle.is_loaded && (!scene_handle.has_camera || !scene_handle.has_light) {
        if meshes.iter().any(|(_, maybe_aabb)| maybe_aabb.is_none()) {
            return;
        }

        let mut min = Vec3A::splat(f32::MAX);
        let mut max = Vec3A::splat(f32::MIN);
        for (transform, maybe_aabb) in meshes.iter() {
            let aabb = maybe_aabb.unwrap();
            // If the Aabb had not been rotated, applying the non-uniform scale would produce the
            // correct bounds. However, it could very well be rotated and so we first convert to
            // a Sphere, and then back to an Aabb to find the conservative min and max points.
            let sphere = Sphere {
                center: Vec3A::from(transform.mul_vec3(Vec3::from(aabb.center))),
                radius: (Vec3A::from(transform.scale) * aabb.half_extents).length(),
            };
            let aabb = Aabb::from(sphere);
            min = min.min(aabb.min());
            max = max.max(aabb.max());
        }

        let size = (max - min).length();
        let aabb = Aabb::from_min_max(Vec3::from(min), Vec3::from(max));

        if !scene_handle.has_camera {
            let bundle = if let Some((transform, camera)) = cameras_3d.iter().next() {
                let mut transform: Transform = (*transform).into();
                let (yaw, pitch, _) = transform.rotation.to_euler(EulerRot::YXZ);
                transform.rotation = Quat::from_euler(EulerRot::YXZ, yaw, pitch, 0.0);
                PerspectiveCameraBundle {
                    camera: camera.clone(),
                    transform,
                    ..PerspectiveCameraBundle::new_3d()
                }
            } else {
                let transform = Transform::from_translation(
                    Vec3::from(aabb.center) + size * Vec3::new(0.5, 0.25, 0.5),
                )
                .looking_at(Vec3::from(aabb.center), Vec3::Y);
                let view = transform.compute_matrix();
                let mut perspective_projection = PerspectiveProjection::default();
                perspective_projection.far = perspective_projection.far.max(size * 10.0);
                let view_projection =
                    view.inverse() * perspective_projection.get_projection_matrix();
                let frustum = Frustum::from_view_projection(
                    &view_projection,
                    &transform.translation,
                    &transform.back(),
                    perspective_projection.far(),
                );
                let camera = Camera {
                    near: perspective_projection.near,
                    far: perspective_projection.far,
                    ..default()
                };
                PerspectiveCameraBundle {
                    camera,
                    perspective_projection,
                    frustum,
                    transform,
                    ..PerspectiveCameraBundle::new_3d()
                }
            };

            info!("Spawning a 3D perspective camera");
            let entity = commands
                .spawn_bundle(bundle)
                .insert(CameraController::default())
                .id();
            active_camera_3d.set(entity);

            scene_handle.has_camera = true;
        }

        if !scene_handle.has_light {
            // The same approach as above but now for the scene
            let sphere = Sphere {
                center: aabb.center,
                radius: aabb.half_extents.length(),
            };
            let aabb = Aabb::from(sphere);
            let min = aabb.min();
            let max = aabb.max();

            info!("Spawning a directional light");
            commands.spawn_bundle(DirectionalLightBundle {
                directional_light: DirectionalLight {
                    shadow_projection: OrthographicProjection {
                        left: min.x,
                        right: max.x,
                        bottom: min.y,
                        top: max.y,
                        near: min.z,
                        far: max.z,
                        ..default()
                    },
                    shadows_enabled: false,
                    ..default()
                },
                ..default()
            });

            scene_handle.has_light = true;
        }
    }
}
*/


fn scene_load_check(
    asset_server: Res<AssetServer>,
    mut scenes: ResMut<Assets<Scene>>,
    gltf_assets: ResMut<Assets<Gltf>>,
    mut scene_handle: ResMut<SceneHandle>,
    mut scene_spawner: ResMut<SceneSpawner>,
) {
    match scene_handle.instance_id {
        None => {
            if asset_server.get_load_state(&scene_handle.handle) == LoadState::Loaded {
                let gltf = gltf_assets.get(&scene_handle.handle).unwrap();
                let gltf_scene_handle = gltf.scenes.first().expect("glTF file contains no scenes!");
                let scene = scenes.get_mut(gltf_scene_handle).unwrap();

                let mut query = scene
                    .world
                    .query::<(Option<&DirectionalLight>, Option<&PointLight>)>();

                scene_handle.has_light =
                    query
                        .iter(&scene.world)
                        .any(|(maybe_directional_light, maybe_point_light)| {
                            maybe_directional_light.is_some() || maybe_point_light.is_some()
                        });

                scene_handle.instance_id =
                    Some(scene_spawner.spawn(gltf_scene_handle.clone_weak()));

                // scene_handle.animations = gltf.animations.clone();
                // if !scene_handle.animations.is_empty() {
                //     info!(
                //         "Found {} animation{}",
                //         scene_handle.animations.len(),
                //         if scene_handle.animations.len() == 1 {
                //             ""
                //         } else {
                //             "s"
                //         }
                //     );
                // }

                dbg!(&scene_handle.has_light);
                info!("Spawning scene...");
            }
        }

        Some(instance_id) if !scene_handle.is_loaded => {
            if scene_spawner.instance_is_ready(instance_id) {
                info!("...done!");
                scene_handle.is_loaded = true;
            }
        }
        Some(_) => {}
    }
}



fn assign_components(mut commands: Commands,
                    mut meshes: ResMut<Assets<Mesh>>,
                    // mut rigid_bodies: ResMut<RigidBodySet>,
                    mut materials: ResMut<Assets<StandardMaterial>>,
                    entities: Query<(Entity, &Name, &Transform), Added<Name>>,
                    // mut scene_handle: ResMut<SceneHandle>,
                    // assets_gltf: Res<Assets<Gltf>>,
                    assets_gltfmesh: Res<Assets<GltfMesh>>,
                )
        {

    let mut unique_entities = HashSet::new();
    for (entity, name, transform) in entities.iter() {
        match name.as_str() {
            // "barricade_low" => {
            //     // dbg!(name.as_str(), &entity, &transform);
            //     // commands.entity(entity).insert(MeshedEntity);
            // },

            // "wallfront" => {
            //     dbg!(name.as_str(), &entity, &transform);
            //     // commands.entity(entity).insert(MeshedEntity);
            //     // dbg!(&transform);
            //     // commands.spawn()
            //     // .insert(RigidBody::Static)
            //     // .insert(CollisionShape::Custom {
            //     //     shape: CustomCollisionShape::new(ColliderBuilder::trimesh(vertices, indices)),
            //     // })
            //     // .insert(
            //     //     CollisionLayers::none()
            //     //         .with_group(Layer::World)
            //     //         .with_masks(Layer::all()),
            //     // );
            //     // commands.spawn()
            //     // // Make it a rigid body
            //     // .insert(RigidBody::Static)
            //     // // Attach a collision shape
            //     // .insert(CollisionShape::Cuboid {
            //     //     half_extends: Vec3::new(5., 5., 5.),
            //     //     border_radius: None,
            //     // })
            //     // // Define restitution (so that it bounces)
            //     // .insert(PhysicMaterial {
            //     //     restitution: 10.5,
            //     //     ..Default::default()
            //     // });
            // },

            "uav3" => {
                // let gltf = assets_gltf.get(&scene_handle.handle).unwrap();
                // if let Some(_uav3) =  assets_gltfmesh.get(&gltf.named_meshes["uav3"]) {
                //     dbg!("HERE");
                //     commands
                //     .entity(entity)
                //     .insert(RigidBody::Static)
                //     .insert(CollisionShape::Sphere { radius: 0.01 })
                //     .insert(PhysicMaterial {
                //         restitution: 1.5,
                //         friction: 1.5,
                //         density: 5.0, // Value must be greater than 0.0
                //         ..Default::default()
                //     })
                //     .insert(Drone);
                // }
                //
                // assets_gltfmesh.get())
                // commands
                // .entity(entity)
                // .insert(RigidBody::Static)
                // .insert(Drone);
                //
                // commands.spawn_bundle(TransformBundle {
                //     local: transform.to_owned(),
                //     global: GlobalTransform::identity(),
                // });

                if !unique_entities.contains("uav3") {
                    // let size = Vec2::new(3.0, 3.0);
                    let transform = transform.to_owned();
                    dbg!("Inserting", name.as_str(), &entity, &transform);

                    commands
                    .entity(entity)
                    .insert(RigidBody::Dynamic)
                    .insert(CollisionShape::Cuboid {
                        half_extends: Vec3::new(1.5, 1.5, 1.5),
                        border_radius: None,
                    })
                    .insert(PhysicMaterial {
                        restitution: 3.,
                        friction: 10.,
                        density: 10.0,
                        ..Default::default()
                    })
                    .insert(
                        CollisionLayers::none()
                            .with_group(Layer::World)
                            .with_masks(Layer::all()),
                    )
                    // .insert(Velocity::default())
                    .insert(Velocity::from(Vec2::X * 50.0)
                            // .with_angular(AxisAngle::new(Vec3::Y, -3.14))
                        )
                    .insert(Drone);

                    unique_entities.insert("uav3");
                    // dbg!(name.as_str(), &entity, &transform);

                }
            }

            _ => {
                commands
                .entity(entity)
                .insert(RigidBody::Static)
                .insert(CollisionShape::Cuboid {
                    half_extends: Vec3::new(1., 1., 1.),
                    border_radius: Some(1.),
                })
                .insert(PhysicMaterial {
                    restitution: 30.,
                    friction: 1.0,
                    density: 100.0,
                    ..Default::default()
                })
                .insert(CollisionLayers::new(Layer::World, Layer::World))
                .insert(Obstacle);



            },
        };
    }
}


fn enable_ccd(
    mut rigid_bodies: ResMut<RigidBodySet>,
    new_handles: Query<&RigidBodyHandle, (With<Drone>, Added<RigidBodyHandle>)>,
) {
    for handle in new_handles.iter() {
        if let Some(body) = rigid_bodies.get_mut(handle.into_rapier()) {
            dbg!("here");
            body.enable_ccd(true);
        }
    }
}

fn log_collisions(mut events: EventReader<CollisionEvent>) {
    for event in events.iter() {
        match event {
            CollisionEvent::Started(d1, d2) => {
                println!("Collision started between {:?} and {:?}", d1, d2)
            }
            CollisionEvent::Stopped(d1, d2) => {
                println!("Collision stopped between {:?} and {:?}", d1, d2)
            }
        }
    }
}


/*
fn assign_colliders_to_meshed_entities(
    // scene_handle: ResMut<SceneHandle>,
    meshes: Res<Assets<Mesh>>,
    mut commands: Commands,
    mut materials: ResMut<Assets<StandardMaterial>>,
    entities: Query<(Entity, &Name, &Handle<Mesh>), Added<Name>>)
    // entities: Query<(&Name), With<MeshedEntity> >)
    {

        // let material_handle = materials.add(StandardMaterial {
        //     base_color: Color::rgb(0.8, 0.7, 0.6),
        //     ..Default::default()
        // });

        /*
        for (_mesh_id, mesh) in meshes.iter() {
            let vertices: Vec<Point3<Real>> = match mesh.clone().attribute(Mesh::ATTRIBUTE_POSITION) {
                None => panic!("Mesh does not contain vertex positions"),
                Some(vertex_values) => match &vertex_values {
                    VertexAttributeValues::Float32x3(positions) => positions
                        .iter()
                        .map(|[x, y, z]| Point3::new(*x, *y, *z))
                        .collect(),
                    _ => panic!("Unexpected types in {:?}", Mesh::ATTRIBUTE_POSITION),
                },
            };

            let indices: Vec<_> = match mesh.indices().unwrap() {
                Indices::U16(_) => {
                    panic!("expected u32 indices");
                }
                Indices::U32(indices) => indices
                    .chunks(3)
                    .map(|chunk| [chunk[0], chunk[1], chunk[2]])
                    .collect(),
            };

            commands
            .spawn()
            .insert(RigidBody::Static)
            .insert(CollisionShape::Custom {
                shape: CustomCollisionShape::new(ColliderBuilder::trimesh(vertices, indices)),
            })
            .insert(
                CollisionLayers::none()
                    .with_group(Layer::World)
                    .with_masks(Layer::all()),
            );

            // info!("Added collision shape of mesh id: {:?}", &mesh_id);
        }
        */
        // for (name) in entities.iter() {

        for (_, name, mesh) in entities.iter() {
                info!("AM I HERE?");
                let material_handle = materials.add(StandardMaterial {
                    base_color: Color::rgb(0.8, 0.7, 0.6),
                    ..Default::default()
                });

                // load mesh
                let single_mesh = meshes.get(mesh).unwrap();
                let vertices: Vec<Point3<Real>> = match single_mesh.clone().attribute(Mesh::ATTRIBUTE_POSITION) {
                    None => panic!("Mesh does not contain vertex positions"),
                    Some(vertex_values) => match &vertex_values {
                        VertexAttributeValues::Float32x3(positions) => positions
                            .iter()
                            .map(|[x, y, z]| Point3::new(*x, *y, *z))
                            .collect(),
                        _ => panic!("Unexpected types in {:?}", Mesh::ATTRIBUTE_POSITION),
                    },
                };
                let indices: Vec<_> = match single_mesh.indices().unwrap() {
                    Indices::U16(_) => {
                        panic!("expected u32 indices");
                    }
                    Indices::U32(indices) => indices
                        .chunks(3)
                        .map(|chunk| [chunk[0], chunk[1], chunk[2]])
                        .collect(),
                };

                dbg!(&vertices, &indices);
                let mm_bundle = PbrBundle {
                    mesh: mesh.clone(),
                    material: material_handle.clone(),
                    transform: Transform::from_xyz(0.0, 0.0, 0.0),
                    ..Default::default()
                };

                commands
                    .spawn()
                    .insert_bundle(mm_bundle)
                    .insert(RigidBody::Static)
                    .insert(CollisionShape::Custom {
                        shape: CustomCollisionShape::new(ColliderBuilder::trimesh(vertices, indices)),
                    })
                    // .insert(LevelAsset::CustomMaterial {
                    //     properties: material_properties,
                    //     handle: material,
                    // })
                    .insert(
                        CollisionLayers::none()
                            .with_group(Layer::World)
                            .with_masks(Layer::all()),
                    );



            // match name.as_str() {
            //     "barricade_low" => {
            //     dbg!("Found rigid body");
            //     info!("Added collision shape of mesh name: {:?}", &name.as_str());
            //     },
            //     _ => println!("Ignoring rigid body for {}", &name.as_str())
            // }
        }


}

*/