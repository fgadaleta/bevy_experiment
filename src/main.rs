use bevy::{
    prelude::*,

    window::{WindowMode, WindowResizeConstraints, PresentMode},
};
// use blender_bevy_toolkit::BlendLoadPlugin;
use game::GamePlugin;
// use debug::DebugPlugin;

// use self::fly_camera::FlyCameraPlugin;
// use follow_camera::FollowCameraPlugin;

mod debug;
// mod fly_camera;
// mod follow_camera;

// const SCENE_DIR: &'static str = "../assets/";

// struct SceneHandler {
//     scene_list: Vec<String>,
//     selected_scene_name: String,
//     current_scene_name: Option<String>,
// }

#[derive(Component)]
struct Preserve {}

#[derive(Component)]
struct SceneListTag {}
#[derive(Component)]
struct CurrentSceneTag {}


fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "game-experiment".to_string(),
            width: 1400.0,
            height: 720.0,
            position: None,
            resize_constraints: WindowResizeConstraints {
                min_width: 256.0,
                min_height: 256.0,
                ..Default::default()
            },
            scale_factor_override: None, //Some(1.0), //Needed for some mobile devices, but disables scaling
            present_mode: PresentMode::Immediate,
            resizable: true,
            decorations: true,
            cursor_locked: false,
            cursor_visible: true,
            mode: WindowMode::Windowed,
            transparent: false,
            #[cfg(target_arch = "wasm32")]
            canvas: None,
        })
        .insert_resource(Msaa { samples: 4 })
        // .add_startup_system(start_game)
        .add_plugins(DefaultPlugins)
        // .add_plugin(DebugPlugin)
        // .add_plugin(smooth_bevy_cameras::LookTransformPlugin)
        .add_plugin(GamePlugin)
        .run();
}
